
## Fluorescent Markov Beat Code ## https://gitlab.com/oscar__noish/mmm-metamutsicmachines

#%%
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt

#%%
#cargo el archivo de texto con los datos del analisis de los intervalos entre beats IOI (Inter Onset Interval)
#de los INPUTS que quiero introducir en el modelo
onsets = np.loadtxt('3.txt')


#%%
#aqui creo un array que resta cada marca del ritmo a la anterior para
#calcular la duración, los espacios entre ritmos 
IOI = []
for i in range(len(onsets)-1):
    ioi_temp = ((onsets[i+1] - onsets[i])* 1000)
    IOI.append(ioi_temp)


#%%
##convierto en enteros 
int_IOI=[]
for intt in IOI:
   int_IOI.append(int(intt))



#%%
np.max(int_IOI)
#%%
np.min(int_IOI)
#%%
hist = plt.hist(int_IOI,392)

# %%
#aqui discretizo el array en un grupo de 16 rangos porque luego lo voy 
#a representar con cuatro flourescentes, dos estados encendido apagado
#16 posiciones distintas (1111,0000,1010,0101,1000,............)
#utilizo de pandas pd.cut 
#copio el array anterior 
df_nums=int_IOI

#%%
#aqui los bins = los rangos 
#y labels los nombres de los rangos 
#!!PUEDE qUE EL FALLO Que da mas adelante este aqui
#por el numero de rangos? el -inf seria 

#bins = [float('-inf'),0.3,0.42,0.49,0.5,0.53,0.57,0.60,0.65,0.70,0.75,0.8,0.85,0.9,1.0,float('inf')]   
#labels = ['1','2','3','4','5','6','7','8','9','10','11','12','13','14','15']
#df_nums = pd.cut(df_nums, bins=bins, labels=labels, include_lowest=False)

bins = [0,300,400,500,600,700,800,900,1000,3000]
labels = ['0','1','2','3','4','5','6','7','8']
df_nums = pd.cut(df_nums, bins=bins, labels=labels)


#%%
print(df_nums)
#%%
hist = plt.hist(df_nums,393)

#%%
#aqui defino la función para crear la función de matriz de transicion para la cadena
#de markov a partir del array discretizado df_nums 

def transition_matrix(transitions):
    n = 1+ max(transitions) #number of states
    
    M = [[0]*n for _ in range(n)]

    for (i,j) in zip(transitions,transitions[1:]):
        M[i][j] += 1

    #now convert to probabilities:
    for row in M:
        s = sum(row)
        if s > 0:
            row[:] = [f/s for f in row]
    return M

#%%
#testeando la función de matriz transición
#transforma a integrales porque no le molaba str..
#aqui si te fijas en la matriz de transicion, la primera fila sale todas a 0
#eso es lo que falla, 
t = df_nums
t = t.astype(np.integer)
m = transition_matrix(t)
for row in m: print(' '.join('{0:.2f}'.format(x) for x in row))


# %%
#aqui creo la clase del generador de markov para generar secuencias 
class MarkovChain(object):
    def __init__(self, transition_matrix, states):
        """
        Initialize the MarkovChain instance.
 
        Parameters
        ----------
        transition_matrix: 2-D array
            A 2-D array representing the probabilities of change of 
            state in the Markov Chain.
 
        states: 1-D array 
            An array representing the states of the Markov Chain. It
            needs to be in the same order as transition_matrix.
        """
        self.transition_matrix = np.atleast_2d(transition_matrix)
        self.states = states
        self.index_dict = {self.states[index]: index for index in 
                           range(len(self.states))}
        self.state_dict = {index: self.states[index] for index in
                           range(len(self.states))}
 
    def next_state(self, current_state):
        """
        Returns the state of the random variable at the next time 
        instance.
 
        Parameters
        ----------
        current_state: str
            The current state of the system.
        """
        return np.random.choice(
         self.states, 
         p=self.transition_matrix[self.index_dict[current_state], :]
        )
 
    def generate_states(self, current_state, no=10):
        """
        Generates the next states of the system.
 
        Parameters
        ----------
        current_state: str
        The state of the current random variable.
 
        no: int
            The number of future states to generate.
        """
        future_states = []
        for i in range(no):
            next_state = self.next_state(current_state)
            future_states.append(next_state)
            current_state = next_state
        return future_states


# %%
#testeo el modelo de markov m la matriz de transición que cree antes 
transition_matrix = m
print(m)

# %%
generadorRYTHM = MarkovChain(transition_matrix=m, states=['0', '1', '2','3', '4', '5','6', '7', '8'])



# %%
generadorRYTHM.next_state(current_state='1')

#%%

secuencia = []

secuencia = generadorRYTHM.generate_states(current_state='0', no=100)


print(secuencia)




# %%
c = np.savetxt('geekfile.txt', secuencia, fmt="%s" )



# %%
#matriz de 3.txt
0.00 1.00 0.00 0.00 0.00 0.00 0.00 0.00 0.00
0.01 0.76 0.06 0.17 0.00 0.00 0.00 0.00 0.00
0.02 0.06 0.49 0.42 0.02 0.00 0.00 0.00 0.00
0.00 0.05 0.12 0.73 0.03 0.05 0.01 0.00 0.00
0.00 0.00 0.12 0.88 0.00 0.00 0.00 0.00 0.00
0.00 0.00 0.00 1.00 0.00 0.00 0.00 0.00 0.00
0.00 0.00 0.00 1.00 0.00 0.00 0.00 0.00 0.00
0.00 0.00 0.00 1.00 0.00 0.00 0.00 0.00 0.00
0.00 0.00 0.00 1.00 0.00 0.00 0.00 0.00 0.00

#secuencia de 100 empezando por cero
...
['1', '1', '1', '1', '1', '1', '3', '3', '3', '3', '1', '1', '1', '1', '1', '1', '1', '1', '2', '1', '1', '1', '1', '3', '2', '3', '3', '3', '3', '2', '2', '3', '3', '3', '2', '2', '1', '1', '1', '1', '1', '1', '1', '3', '3', '2', '3', '3', '3', '4', '3', '3', '3', '1', '1', '1', '1', '1', '1', '1', '3', '1', '1', '1', '1', '3', '3', '3', '3', '1', '3', '3', '3', '3', '3', '3', '6', '3', '3', '3', '3', '3', '3', '5', '3', '2', '3', '4', '3', '3', '1', '1', '1', '1', '1', '1', '1', '1', '1', '3']


##
